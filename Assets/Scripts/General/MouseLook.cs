﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public Transform playerBody;

    public float mouseSensitiviy = 100.0f;
    
    float xRotation = 0f;
    private void Start() {
        //Cursor.lockState = CursorLockMode.Locked;

    }
     void Update()
    {
        float mouseX = (Input.GetAxis("Mouse X")) * mouseSensitiviy * Time.deltaTime;
        float mouseY = (Input.GetAxis("Mouse Y")) * mouseSensitiviy * Time.deltaTime;

        // float mouseX = (Input.GetAxis("Controller X2")) * mouseSensitiviy * Time.deltaTime;
        // float mouseY = (Input.GetAxis("Controller Y2")) * mouseSensitiviy * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90.0f, 90.0f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);

    }
}
