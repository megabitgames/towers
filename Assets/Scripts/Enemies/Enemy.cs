﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed = 3.0f; 
    public float health = 100.0f;
    public float physicalShield = 0.0f;
    public float magicalShield = 0.0f;

    bool isDead = false;
    PathFinding pathfinder;
    PathFollow pathFollower;
    // Start is called before the first frame update
    public Transform testPoint;
    private void Awake() {
        pathfinder = GetComponent<PathFinding>();
        pathFollower = GetComponent<PathFollow>(); 
        pathfinder.SetTarget(testPoint);
        PathFinding.OnPathFound += PathFoundHandler;
        
    }
    public void TakeDamage(float damage, DamageType dType)
    {
        if (isDead) return;
        float rawDamage = 0.0f;
        if (dType == DamageType.Physical)
        {
            physicalShield -= damage;
            if (physicalShield < 0.0f)
            {
                rawDamage = physicalShield * -1.0f; // Use any damage above shield level as raw damage now
                physicalShield = 0.0f;
            }
        }else if (dType == DamageType.Magical)
        {
            magicalShield -= damage;
            if (magicalShield < 0.0f)
            {
                rawDamage = magicalShield * -1.0f; // Use any damage above shield level as raw damage now
                magicalShield = 0.0f;
            }
        }
        health -= rawDamage;
        if (health <= 0.0f)
        {
            health = 0.0f;
            Die();
        }

    }
    public void Die()
    {
        isDead = true;
        pathFollower.ClearPath(); //Stop any movement
        Destroy(gameObject);

        
    }
    private void Update() {
        // TakeDamage(Time.deltaTime * 20.0f, DamageType.Physical);
        // TakeDamage(Time.deltaTime * 20.0f, DamageType.Magical);
    }
    public void PathFoundHandler(List<Vector3> pathPositions, PathFinding pathfindingInstance)
    {
        if (pathfindingInstance != pathfinder) return;
        CreatePathFromPositions(pathPositions);
    }

    void CreatePathFromPositions(List<Vector3> positions)
    {        
        // animator.SetBool("IsWalking", true);
        foreach (Vector3 pos in positions)
        {
            pathFollower.AddPosition(pos);
        }
        pathFollower.Start();
    }
}
