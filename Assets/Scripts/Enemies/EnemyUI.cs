﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyUI : MonoBehaviour
{
    public Transform healthFill;
    public Transform magicFill;
    public Transform physicalFill;
    Enemy enemy;
    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponent<Enemy>();        
    }

    // Update is called once per frame
    void Update()
    {
        healthFill.localScale = new Vector3(enemy.health / 100.0f, 1.0f, 1.0f);
        magicFill.localScale = new Vector3(enemy.magicalShield / 100.0f, 1.0f, 1.0f);
        physicalFill.localScale = new Vector3(enemy.physicalShield / 100.0f, 1.0f, 1.0f);
    }
}
