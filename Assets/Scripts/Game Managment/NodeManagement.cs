﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManagement : Singleton<NodeManagement>
{
    public GameObject TileUserInterface;
    public BuildableBlueprint[] blueprints;
    [HideInInspector] public bool menuOpened = false; 
    Tile selectedTile;
    // Update is called once per frame
    public bool IsActiveTile(Tile t){
        return selectedTile == t;
    }
    protected NodeManagement()
    {

    }
    private void Awake() {
        TileUserInterface.SetActive(false);
    }
    public void ToggleTile(Tile t)
    {
        if(selectedTile == t){
            DeselectTile(t);
        }else{
            SelectTile(t);
        }
    }
    public void SelectTile(Tile t)
    {
        if (selectedTile != null) DeselectTile(selectedTile);

        selectedTile = t;
        t.NodeSelected();
    }
    public void ShowUserInterfaceForTile(Tile t)
    {
        menuOpened = true;
        //TileUserInterface.transform.position = t.transform.position;
        TileUserInterface.SetActive(true);
    }
    public void CloseInterface()
    {
        DeselectTile(selectedTile);
    }
    public void DeselectTile(Tile t)
    {
        t.NodeDeselected();
        selectedTile = null;
        TileUserInterface.SetActive(false);
        menuOpened = false;
    }
    public void PlaceItem(string itemKey)
    {
        foreach(BuildableBlueprint blueprint in blueprints)
        {
            if (blueprint.key == itemKey){
                PlaceItem(blueprint);
                return;
            }
        }
    }
    void PlaceItem(BuildableBlueprint blueprint)
    {
        if (selectedTile == null) return;
        selectedTile.PlaceItem(blueprint);
        DeselectTile(selectedTile);
    }
}
