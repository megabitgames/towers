﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour
{
  public Color hoverColor;
    
    Turret currentItem; //Will need to change class to whatever I use as a base class for buildable items eventually
    Color startColor;
    Renderer myRenderer;

    private void Start() {
        myRenderer = GetComponent<Renderer>();
        startColor = myRenderer.material.color;
    }
    private void OnMouseEnter() {        
        if (EventSystem.current.IsPointerOverGameObject() || NodeManagement.Instance.menuOpened)
            return;
        myRenderer.enabled = true;        
        // myRenderer.material.color = hoverColor; 
    }
    // private void Update() {
    //     if (Input.GetMouseButtonDown(0))
    //     {
    //         Debug.Log("Clicked");
    //     }
    // }
    private void OnMouseExit() {
        myRenderer.enabled= false;
        // myRenderer.material.color = startColor;
    }
    private void OnMouseDown() {
        if (EventSystem.current.IsPointerOverGameObject() || NodeManagement.Instance.menuOpened)
            return;
        NodeManagement.Instance.ToggleTile(this);
    }
    public void SetItem(Turret item)
    {
        currentItem = item;
    }
    public void RemoveActiveItem()
    {
        if (currentItem == null) return;

        Destroy(currentItem);
        currentItem = null;
    }
    public void NodeSelected()
    {
        if (currentItem == null){
            ShowBuildMenu();
        }else{
            ShowDetails();
        }
    }
    public void PlaceItem(BuildableBlueprint itemBlueprint)
    {
        if (currentItem != null) return;

        GameObject obj = Instantiate(itemBlueprint.prefab, transform.position, Quaternion.identity);
        currentItem = obj.GetComponent<Turret>();

    }
    void ShowBuildMenu()
    {
        NodeManagement.Instance.ShowUserInterfaceForTile(this);
    }
    void ShowDetails()
    {
        currentItem.userInterface.SetActive(true);
        currentItem.rangeIndication.SetActive(true);
    }
    public void NodeDeselected()
    {
        if (currentItem == null) return;

        currentItem.userInterface.SetActive(false);
        currentItem.rangeIndication.SetActive(false);

    }
    public Vector3 GetBuildPosition()
    {
        return transform.position + new Vector3(0.0f, 0.5f, 0.0f);
    }
}
