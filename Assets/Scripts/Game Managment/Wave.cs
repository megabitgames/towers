﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Wave 
{
    public WaveComponent[] config;
    // Start is called before the first frame update

}
[System.Serializable]
public class WaveComponent
{
    public GameObject enemyPrefab;
    public int quantity;
    // Start is called before the first frame update

}
