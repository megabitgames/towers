﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public GameObject impactPrefab;
    Enemy target;
    bool targetHit = false;
    float damage;
    float speed;
    float lifetime = 5.0f;
    float damageRadius;
    DamageType damageType;

    public void ShootAtTarget(Enemy e, float _damage = 20.0f, float _speed = 50.0f, float _damageRadius = 0.0f, DamageType _damageType = DamageType.Physical)
    {
        target = e;
        damage = _damage;
        speed = _speed;
        damageRadius = _damageRadius;
        damageType = _damageType;
    }
    void SelfDestruct()
    {
        Destroy(gameObject);

    }
    void DamageEnemy(Enemy enemy)
    {
        enemy.TakeDamage(damage, damageType);
    }
    void HitTarget()
    {
        targetHit = true;
        GameObject impact = Instantiate(impactPrefab, transform.position, transform.rotation);
        Destroy(impact, 1.0f);
        DamageEnemy(target); //May pass more if i use explosion
        SelfDestruct();
    }
    private void Update() {
        if (targetHit) return;

        if (lifetime <= 0.0f || target == null)
        {
            SelfDestruct();
            return;
        }else{
            lifetime -= Time.deltaTime;
            Vector3 dir = (target.transform.position - transform.position);     
            float distanceThisFrame = Time.deltaTime * speed;
            if (dir.magnitude <= distanceThisFrame)
            {
                HitTarget();
            }else{
                transform.Translate(dir.normalized * distanceThisFrame, Space.World);
                transform.LookAt(target.transform);
            }
        }

    }

}
