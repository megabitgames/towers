﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public float range = 15.0f;
    public float turnSpeed = 10.0f;
    public float fireRate = 1.0f;
    public float damage = 20.0f;
    public Transform rotationComponent; 
    public Transform bulletSpawnPoint;

    public GameObject userInterface;
    public GameObject bullet;
    public GameObject bulletShotEffect;
    public GameObject rangeIndication;

    string enemyTag = "Enemy";
    Enemy currentTarget;
    float fireCountdown = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        ShowRange();
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }
    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        Enemy nearestEnemy = null;
        foreach(GameObject enemyObj in enemies)        
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemyObj.transform.position);
            if (distanceToEnemy < range && distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemyObj.GetComponent<Enemy>();                
            }
        }

        currentTarget = nearestEnemy;
    }

    void ShowRange()
    {
        rangeIndication.transform.localScale = new Vector3(range, range, range);
        rangeIndication.SetActive(true);   
        StartCoroutine(HideRangeAfterDelay());

    }
    IEnumerator HideRangeAfterDelay()
    {
        yield return new WaitForSeconds(1.5f);
        rangeIndication.SetActive(false);
    }
    void Shoot()
    {
        fireCountdown -= Time.deltaTime;

        if (fireCountdown <= 0.0f)
        {
            if (Vector3.Distance(transform.position, currentTarget.transform.position) < range)
            {
                fireCountdown = 1.0f/fireRate;
                Debug.Log("Shooting");
                GameObject bObj = Instantiate(bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
                Bullet b = bObj.GetComponent<Bullet>();
                b.ShootAtTarget(currentTarget);

                GameObject effect = Instantiate(bulletShotEffect, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
                Destroy(effect, 1.0f);
            }
        }
    }
    private void Update() {            
        if (currentTarget != null){
            RotateToTarget();
            Shoot();
        }
    }
    void RotateToTarget()
    {   
        Debug.Log("Rotating");
        Vector3 dir = (currentTarget.transform.position - transform.position);
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        lookRotation = Quaternion.Lerp(rotationComponent.rotation, lookRotation, Time.deltaTime * turnSpeed);
        Vector3 rotationVector = lookRotation.eulerAngles;
        rotationComponent.rotation = Quaternion.Euler(0f, rotationVector.y, 0f);
    }}
