﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollow : MonoBehaviour
{
    public float distanceThreshold = 0.05f;
    public Vector3 currentTargetPosition;
    public bool isCompleted = false;
    Enemy enemy;
    bool hasReachedTarget = false;
    bool targetHasBeenSet = false;
    List<Vector3> pathPositions = new List<Vector3>();

    public void ClearPath()
    {
        pathPositions.Clear();
        isCompleted = true;
        hasReachedTarget = true;
        targetHasBeenSet = false;

    }
    public void AddPosition(Vector3 pos)       
    {
        pathPositions.Add(pos);
    }

    public void Start()
    {
        enemy = GetComponent<Enemy>();

        if (pathPositions.Count > 0){
            currentTargetPosition = pathPositions[0];
            isCompleted = false;
            hasReachedTarget = false;
            targetHasBeenSet = true;
        }else{
            ClearPath();
        }
    }
    void ProgressPath()
    {
        if (pathPositions.Count > 0)
        {
            pathPositions.RemoveAt(0);
        }
        if (pathPositions.Count > 0)
        {
            Vector3 pos = pathPositions[0];
            currentTargetPosition = new Vector3(pos.x, transform.position.y, pos.z);
            hasReachedTarget = false;
        }
        else
        {
            CompletePath();
        }
    }
    void CompletePath()
    {
        //Attack the tree?
        isCompleted = true;
    }
    public void Update()
    {
        if (!hasReachedTarget && targetHasBeenSet)
        {
            Vector3 modifiedTarget = new Vector3(currentTargetPosition.x, transform.position.y, currentTargetPosition.z);
            float currentDistance = Vector3.Distance(transform.position, modifiedTarget);
            Vector3 movement = (modifiedTarget - transform.position).normalized * enemy.speed * Time.deltaTime;
            Vector3 newPos = transform.position + movement;
            float newDistance = Vector3.Distance(newPos, modifiedTarget);
            if (Vector3.Distance(newPos, modifiedTarget) < distanceThreshold || newDistance > currentDistance)
            {
                newPos = modifiedTarget;
            }
            transform.transform.position = Vector3.Lerp(transform.transform.position, newPos, 0.7f);
            if (Vector3.Distance(transform.position, modifiedTarget) < distanceThreshold)
            {
                hasReachedTarget = true;
                ProgressPath();
            }
        }
    }
    
}
