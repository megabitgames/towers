﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFoundEventArgs
{

}
public class PathFinding : MonoBehaviour
{
    //Event Stuff
    public delegate void PathFound(List<Vector3> pathPositions, PathFinding pathfindingInstance);
    public static event PathFound OnPathFound;
    MovementGrid grid;
    public Transform StartPosition;
    public Transform TargetPosition;
    // Start is called before the first frame update
    void Awake()
    {
        grid = FindObjectOfType<MovementGrid>();
    }

    // Update is called once per frame
    void Update()
    {
        if (TargetPosition != null)
        {
            FindPath(TargetPosition.position);
        }

    }
    void FindPath(Vector3 a_targetPos)
    {
        Node startNode = grid.NodeFromWorldPosition(transform.position);
        Node targetNode = grid.NodeFromWorldPosition(a_targetPos);

        //startNode.IsWall = true;
        //targetNode.IsWall = true;

        List<Node> openList = new List<Node>();
        HashSet<Node> closedList = new HashSet<Node>();

        openList.Add(startNode);
        while (openList.Count > 0)
        {
            Node currentNode = openList[0];
            for(int i = 1; i < openList.Count; i++)
            {
                if (openList[i].FCost < currentNode.FCost || openList[i].FCost == currentNode.FCost && openList[i].hCost < currentNode.hCost)
                {
                    currentNode = openList[i];
                }
            }
            openList.Remove(currentNode);
            closedList.Add(currentNode);
            if (currentNode == targetNode)
            {
                GetFinalPath(startNode, targetNode);
                break;
            }
            foreach (Node neighborNode in grid.GetNeighboringNodes(currentNode))
            {
                /*Skip in cost if this can't be navigated*/
                if ((!neighborNode.isNavigatable) || closedList.Contains(neighborNode))
                {
                    continue;
                }
                int moveCost = currentNode.gCost + GetManhattenDistance(currentNode, neighborNode);
                if (moveCost < neighborNode.gCost || !openList.Contains(neighborNode))
                {
                    neighborNode.gCost = moveCost;
                    neighborNode.hCost = GetManhattenDistance(neighborNode, targetNode);
                    neighborNode.Parent = currentNode;
                    
                    if (!openList.Contains(neighborNode) || moveCost < neighborNode.FCost)
                    {
                        openList.Add(neighborNode);
                    }
                }

            }
        }
    }
    void SetMovementPath(List<Node> path)
    {
        if (path.Count <= 0) return;
        List<Vector3> pathPositions = new List<Vector3>();
        Vector3 positionBeingHeld = Vector3.zero;
        pathPositions.Add(path[0].position);
        bool holdingX = false;
        bool holdingZ = false;
        for (int i = 1; i < (path.Count); i++)
        {
                        Node currentNode = path[i];
            Node previousNode = path[i - 1];
            if (currentNode.gridX == previousNode.gridX)
            {
                if (!holdingZ)
                {
                    holdingX = true;
                }
                else
                {
                    holdingZ = false;
                    pathPositions.Add(previousNode.position);
                }

            }
            else if (currentNode.gridZ == previousNode.gridZ)
            {
                if (!holdingX)
                {
                    holdingZ = true;
                }
                else
                {
                    holdingX = false;
                    pathPositions.Add(previousNode.position);
                }
            }

            if (!holdingX && !holdingZ)
            {
                pathPositions.Add(path[i].position);
            }
        }
        pathPositions.Add(TargetPosition.position);
        if (OnPathFound != null)
        {
            OnPathFound(pathPositions, this);
        }

    }
    public void SetTarget(Transform t)
    {
        TargetPosition = t;
    }
    void GetFinalPath(Node a_StartNode, Node a_EndNode)
    {
        List<Node> finalPath = new List<Node>();
        Node currentNode = a_EndNode;
        while (currentNode != a_StartNode)
        {
            finalPath.Add(currentNode);
            currentNode = currentNode.Parent;
        }
        finalPath.Reverse();
        grid.finalPath = finalPath;
        SetMovementPath(finalPath);
        TargetPosition = null;
    }
    int GetManhattenDistance(Node a_NodeA, Node a_NodeB)
    {
        int ix = Mathf.Abs(a_NodeA.gridX - a_NodeB.gridX);
        int iz = Mathf.Abs(a_NodeA.gridZ - a_NodeB.gridZ);

        return ix +iz;
    }
}
