﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathElement : MonoBehaviour
{
    public List<Transform> NavigatableTransforms = new List<Transform>();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        foreach (Transform t in NavigatableTransforms)
        {
            Gizmos.DrawSphere(t.position, 0.25f);
        }
    }
}
