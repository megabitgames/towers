﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementGrid : MonoBehaviour
{
    [HideInInspector] public Transform startPosition;
    public LayerMask wallMask;
    public LayerMask seatMask;
    public Vector3 gridWorldSize;
    public float nodeRadius;
    public float distance;

    Node[,] grid;
    public List<Node> finalPath;

    float nodeDiameter;
    int gridSizeX, gridSizeZ;

    void Start()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeZ = Mathf.RoundToInt(gridWorldSize.z / nodeDiameter);
        CreateGrid();
    }
    void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeZ];
        Vector3 bottomLeft = transform.position - Vector3.right * ((gridWorldSize.x / 2) + distance) - Vector3.forward * ((gridWorldSize.z / 2) + distance);
        string[] wallLayers = { "MovementGridLayer" };
        string[] tableLayers = { "MovementGridLayer" };
        string[] seatLayers = { "SeatLayer" };
        int wallTapMask = LayerMask.GetMask(wallLayers);
        int tableTapMask = LayerMask.GetMask(tableLayers);
        int seatTapMask = LayerMask.GetMask(seatLayers);



        for (int x = 0; x < gridSizeX; x++)
        {
            for (int z = 0; z < gridSizeZ; z++)
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward *(z*nodeDiameter + nodeRadius);
                // bool wall = false;
                // bool seat = false;

                // Vector3 position3D = new Vector3(worldPoint.x, 0, worldPoint.y);

                // RaycastHit2D wallHit = Physics2D.Raycast(position3D, Vector2.zero, 0.0f, wallTapMask);
                // RaycastHit2D seatHit = Physics2D.Raycast(position3D, Vector2.zero, 0.0f, seatTapMask);
                // RaycastHit2D tableHit = Physics2D.Raycast(position3D, Vector2.zero, 0.0f, tableTapMask);

                // if (wallHit)
                // {
                //     if (wallHit.collider.CompareTag("Wall"))
                //     {
                //         wall = true;
                //     }
                // }
                // if (tableHit)
                // {
                //     if (tableHit.collider.CompareTag("Table"))
                //     {
                //         wall = true;
                //     }
                // }
                // if (seatHit)
                // {
                //     if (seatHit.collider.CompareTag("Seat"))
                //     {
                //         seat = true;
                //         wall = false;
                //     }
                // }
                grid[x, z] = new Node(true, worldPoint, x, z);
            }
        }
    }
    public Node NodeFromWorldPosition(Vector3 a_worldPosition)
    {
        float xPoint = ((a_worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x);
        float zPoint = ((a_worldPosition.z + gridWorldSize.z / 2) / gridWorldSize.z);

        xPoint = Mathf.Clamp01(xPoint);
        zPoint = Mathf.Clamp01(zPoint);

        int x = Mathf.RoundToInt((gridSizeX - 1) * xPoint);
        int z = Mathf.RoundToInt((gridSizeZ - 1) * zPoint);

        return grid[x, z];

    }
    public List<Node> GetNeighboringNodesWithDiag(Node a_Node)
    {
        List<Node> neighboringNodes = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int z = -1; z <= 1; z++)
            {
                if (x == 0 && z == 0)
                {
                    continue;
                }
                int xCheck = a_Node.gridX + x;
                int zCheck = a_Node.gridZ + z;
                if (xCheck >= 0 && xCheck < gridSizeX && zCheck >= 0 && zCheck < gridSizeZ)
                {
                    neighboringNodes.Add(grid[xCheck, zCheck]);
                }
            }
        }
        return neighboringNodes;

    }
    public List<Node> GetNeighboringNodes (Node a_Node)
    {
        List<Node> neighboringNodes = new List<Node>();
        int xCheck;
        int zCheck;

        //Right Side
        xCheck = a_Node.gridX + 1;
        zCheck = a_Node.gridZ;
        if (xCheck >= 0 && xCheck < gridSizeX)
        {
            if (zCheck >= 0 && zCheck < gridSizeZ)
            {
                neighboringNodes.Add(grid[xCheck, zCheck]);
            }
        }
        //Left Side
        xCheck = a_Node.gridX - 1;
        zCheck = a_Node.gridZ;
        if (xCheck >= 0 && xCheck < gridSizeX)
        {
            if (zCheck >= 0 && zCheck < gridSizeZ)
            {
                neighboringNodes.Add(grid[xCheck, zCheck]);
            }
        }
        //Top Side
        xCheck = a_Node.gridX;
        zCheck = a_Node.gridZ + 1;
        if (xCheck >= 0 && xCheck < gridSizeX)
        {
            if (zCheck >= 0 && zCheck < gridSizeZ)
            {
                neighboringNodes.Add(grid[xCheck, zCheck]);
            }
        }
        //Bottom Side
        xCheck = a_Node.gridX;
        zCheck = a_Node.gridZ - 1;
        if (xCheck >= 0 && xCheck < gridSizeX)
        {
            if (zCheck >= 0 && zCheck < gridSizeZ)
            {
                neighboringNodes.Add(grid[xCheck, zCheck]);
            }
        }
        return neighboringNodes;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, gridWorldSize.y, gridWorldSize.z));
        if (grid != null)
        {
            foreach (Node node in grid)
            {
                if (node.isWall)
                {
                    Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.8f);
                    //Gizmos.color = Color.white;
                }
                else if (node.isSeat)
                {
                    Gizmos.color = Color.white;
                }
                else
                {
                    Gizmos.color = new Color(0.0f, 0.15f, 0.0f, 0.15f);
                }
                if (finalPath != null)
                {
                    if (finalPath.Contains(node))
                    {
                        Gizmos.color = new Color(0.0f, 1.0f, 1.0f, 0.5f);
                    }
                }
                Gizmos.DrawCube(node.position, Vector3.one * (nodeDiameter - distance));
            }
        }
    }

}
