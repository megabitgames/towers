﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public bool walkwayPoint = false;

    public int gridX;
    public int gridZ;
    public bool isWall;
    public bool isSeat;
    public bool isNavigatable;
    public Vector3 position;
    public Node Parent;

    public int gCost;
    public int hCost;
    public int FCost
    {
        get { return gCost + hCost; }
    }

    public Node(bool canNav, Vector3 a_pos, int a_gridX, int a_gridZ)
    {
        isNavigatable = canNav;
        position = a_pos;
        gridX = a_gridX;
        gridZ = a_gridZ;
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawSphere(transform.position, 0.25f);
    //}
}
